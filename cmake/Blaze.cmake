########################################################################
# Blaze
########################################################################

# Download Blaze
include(DownloadProject)
download_project(
  PROJ              Blaze
  GIT_REPOSITORY    https://bitbucket.org/blaze-lib/blaze.git
  TIMEOUT           180
  PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
  ${UPDATE_DISCONNECTED_IF_AVAILABLE}
  )

# Process Blaze project (only run configure command and use Blaze as header-only library)
if (NOT TARGET blaze)
  
  # Blaze depends on the Boost library
  find_package(Boost COMPONENTS
    system
    thread
    QUIET REQUIRED)
  include_directories(${Boost_INCLUDE_DIRS})
  set(LIBS ${LIBS} ${Boost_LIBRARIES})
  
  # Blaze depends on Threads
  set(THREADS_PREFER_PTHREAD_FLAG ON)
  find_package(Threads QUIET REQUIRED)
  
  # Detect version of the Blaze library
  if(CMAKE_BUILD_TYPE STREQUAL "Release" OR CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
    set(BLAZE_VERSION "release")
  else()
    set(BLAZE_VERSION "debug")
  endif()
  
  # Detect compiler and compiler flags
  set(BLAZE_CXX ${CMAKE_CXX_COMPILER})
  if(CMAKE_BUILD_TYPE STREQUAL "Release")
    set(BLAZE_CXXFLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELASE}")
  elseif(CMAKE_BUILD_TYPE STREQUAL "Debug")
    set(BLAZE_CXXFLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG}")
  elseif(CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
    set(BLAZE_CXXFLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
  endif()
  
  # Detect static/shared library
  if(BUILD_SHARED_LIBS)
    set(BLAZE_LIBRARY "shared")
  else()
    set(BLAZE_LIBRARY "static")
  endif()
  
  # Detect Boost configuration
  set(BLAZE_BOOST_INCLUDE_PATH ${Boost_INCLUDE_DIRS})
  set(BLAZE_BOOST_LIBRARY_PATH ${Boost_LIBRARY_DIRS})
  
  # Detect size of outermost cache
  if(APPLE)
    execute_process(
      COMMAND sysctl hw.l3cachesize
      COMMAND awk "{print $(NF)}"
      OUTPUT_VARIABLE BLAZE_CACHE_SIZE)
  elseif(UNIX)
    execute_process(
      COMMAND cat /proc/cpuinfo
      COMMAND grep "cache size"
      COMMAND uniq
      COMMAND awk "{print $(NF-1)*1024}"
      OUTPUT_VARIABLE BLAZE_CACHE_SIZE)
  else()
    set(BLAZE_CACHE_SIZE 3145728)
  endif()
  
  # Detect BLAS library
  find_package(BLAS QUIET)
  if(BLAS_FOUND)
    set(BLAZE_BLAS "yes")
    #set(BLAZE_BLAS_INCLUDE_PATH)
    #set(BLAZE_BLAS_INCLUDE_FILE)
    #set(BLAZE_BLAS_IS_PARALLEL)
  else()
    set(BLAZE_BLAS "no")
  endif()
  
  # Detect MPI
  find_package(MPI QUIET)
  if (MPI_FOUND)
    set(BLAZE_MPI "yes")
    set(BLAZE_MPI_INCLUDE_PATH ${MPI_CXX_INCLUDE_PATH})
  else()
    set(BLAZE_MPI "no")
  endif()
  
  configure_file(${CMAKE_SOURCE_DIR}/patches/Configfile.blaze.in ${Blaze_SOURCE_DIR}/Configfile)
  add_custom_target(blaze
    COMMAND ${Blaze_SOURCE_DIR}/configure
    WORKING_DIRECTORY ${Blaze_SOURCE_DIR})
endif()

# Add include directory
include_directories("${Blaze_SOURCE_DIR}")
