#pragma once
#ifndef ODESOLVER_HPP
#define ODESOLVER_HPP

namespace OdeSolver
{
  /** @brief Generic ODE solver
   *
   *  All methods and parameters used by all solver types go here
   *
   *  @tparam T The default type for scalar parameters
   */
  template<typename T>
  class GenericSolver
  {
  public:
    /// Constructor
    GenericSolver()
      : nsteps(0)
    {}
    
    /// Get total number of time steps
    std::size_t getSteps() const
    {
      return nsteps;
    }

    /// Get current simulation time
    T getTime() const
    {
      return time;
    }
    
  protected:
    /// Total number of time steps
    std::size_t nsteps;

    /// Current simulation time
    T time;
  };
  
  /** @brief Forward Euler ODE solver
   *
   *  Specialized implementation of an explicit Forward Euler solver
   *
   *  @tparam T          The default type for scalar parameters
   *  @tparam neq        The number of equations Yi(t)     = [v0(t),v1(t),...,vneq(t)]
   *  @tparam nvar       The number of variables dYi(t)/dt = F(t,Y0(t),Y1(t),...,Ynvar(t)) 
   *  @tparam OdeFunctor The functor implementing function F(t,...)
   */
  template<typename T, std::size_t neq, std::size_t nvar, class OdeFunctor>
  class ForwardEulerSolver : public GenericSolver<T>
  {
  public:
    
    /** @brief Performs one or more steps
     *
     *  @tparam nsteps     The number of steps to be performed
     *  @tparam stepsize   The time-step size to be used
     *  @tparam VectorType The vector type storing [Y0,...,Yneq]
     */

    template<class VectorType>
    void step(const T stepsize, VectorType &Y)
    {
      /// Create temporal vector
      VectorType Ytemp(Y);
      
      /// Loop over all variables
      for (std::size_t ivar=0; ivar<nvar; ivar++)
        {
          /// Update each variable separately
          OdeFunctor::eval(ivar, stepsize, this->time, Ytemp, Y);
        }
      
      /// Update step counter
      this->nsteps++;
      
      /// Update simulation time
      this->time += stepsize;
    }
  };
  
} // namespace OdeSolver

#endif // ODESOLVER_HPP
