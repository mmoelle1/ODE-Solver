#include <array>
#include <iostream>
#include <stdexcept>

#include <blaze/Math.h>
#include "OdeSolver.hpp"

/** @brief Demo functor
 *
 *  This demo functor implements the following ODE system
 *
 *                           /                                      \
 *                           | sin(Y0(t)) + cos(Y1(t)) + tan(Y2(t)) |
 *                           |                                      |
 *  F(t,Y0(t),Y1(t),Y2(t)) = | cos(Y0(t)) + sin(Y1(t)) + tan(Y2(t)) |
 *                           |                                      |
 *                           | exp(Y0(t)) + sin(Y1(t)) + cos(Y2(t)) |
 *                           \                                      /
 *
 *  That is, the system consists of nvar=3 variables Y0(t),Y1(t),Y2(t).
 *  Each variable itself contains neq equations, that is
 *    Yi(t)=[v0(t),v1(t),...,vneq(t)]

 *  For the moment it is assumed that all three types of variables
 *  interact with each other but that the equations are not
 *  coupled. If that assumption does not hold, the definition of the
 *  OdeFunctor would need some sparse matrix vector multiplication.
 */
struct OdeFunctor
{
  /** @brief Evaluation of the OdeFunctor
   *
   *  @tparam VectorType The vector type storing [Y0,...,Yneq]
   */
  template<typename T, class VectorType>
  static void eval(const std::size_t ivar,
                   const T stepsize,
                   const T time,
                   const VectorType Yin,
                   VectorType &Yout)
  {
    switch(ivar) {
    case 0 : Yout[ivar] = Yin[ivar] + stepsize * (sin(Yin[0]) + cos(Yin[1]) + tan(Yin[2]));
      break;
    case 1 : Yout[ivar] = Yin[ivar] + stepsize * (cos(Yin[0]) + sin(Yin[1]) + tan(Yin[2]));
      break;
    case 2 : Yout[ivar] = Yin[ivar] + stepsize * (exp(Yin[0]) + sin(Yin[1]) + cos(Yin[2]));
      break;
    default:
      throw std::runtime_error("Invalid IVAR value.");
      break;
    }
  }; 
};


int main()
{
  const std::size_t nvar = 3;
  const std::size_t neq  = 10;
  
  typedef double real_t;
  typedef blaze::DynamicVector<real_t> vector;
  
  OdeSolver::ForwardEulerSolver<real_t,neq,nvar,OdeFunctor> fe;
  
  std::array<vector,nvar> U = { { vector(neq),
                                  vector(neq),
                                  vector(neq) } };

  /// Initialization
  U[0] = 1;
  U[1] = 2;
  U[2] = 3;
  
  /// Single step with the Forward Euler solver
  fe.step((double)0.3, U);

  /// Output
  std::cout << U[0] << U[1] << U[2];

  /// Another single step with the Forward Euler solver
  fe.step((double)0.3, U);

  /// Output
  std::cout << U[0] << U[1] << U[2];
  
  return 0;
}
